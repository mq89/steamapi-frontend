/**
 * Requires:
 *  - jQuery
 *  - jQuery Mousewheel
 */

function JSScroll() {

    var self = this;
    // Scroll Index
    self.scrollIndex = 0;

    // Positions for scrolling with the mouse.
    self.scrollPositions = $("a[name]");

    /**
    * Scroll from view to view with the mouse wheel.
    */
    $(window).mousewheel(function(e){
        e.preventDefault();
        
        if (e.deltaY < 0){
            // down
            self.scrollIndex++;
        } else {
            // up
            self.scrollIndex--;
        }
        updatePosition();
    });

    /**
     * Scroll to current position when window is resized.
     */
    window.onresize = function(event) {
        updatePosition();
    }

    /*
     * Update page position after scrollIndex has been changed.
     */
    function updatePosition(){   
        if (self.scrollPositions.length > 0) {

            // lower bound
            if (self.scrollIndex < 0){
                self.scrollIndex = 0;
                scroll = false;
            }

            // upder bound
            if (self.scrollIndex >= self.scrollPositions.length){
                self.scrollIndex = self.scrollPositions.length - 1;
            }
            var pos = self.scrollPositions[self.scrollIndex];
            if (window.scrollY != pos.offsetTop) {
                $('html, body').animate({
                    scrollTop: pos.offsetTop
                }, 500);
                //window.location.hash = pos.name;
            }
        }
    }


    /**
     * React to keyboard input.
     */
    $(document).keydown(function(e){
        switch(e.which) {
            case 38: // up
            case 33: // Pg up
                // up
                self.scrollIndex--;
                break;
            case 34: // Pg down
            case 40: // down
                // down
                self.scrollIndex++;
                break;
            case 32: // space
                // goto next
                self.scrollIndex = (self.scrollIndex + 1) % self.scrollPositions.length
                break;
            default:
                return;
        }
        e.preventDefault();
        updatePosition();
    });
}