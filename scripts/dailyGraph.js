dailyGraphOptions = {
    title: {
        text: "Tägliche Spielzeit",
        fontSize: 14
    },
    axisX: {
        title: "Tag",
        valueFormatString: "DDD DD. MMM",
        labelAngle: -45,
        interval: 1,
        intervalType: "day",
        maximum: new Date(),
        labelFontSize: 10,
        titleFontSize: 12
    },
    axisY: {
        title: "Spielzeit",
        valueFormatString: "0.0 h",
        minimum: 0,
        labelFontSize: 10,
        titleFontSize: 12
    },
    data: [
    {
        type: "column",
        color: "#123456",
        xValueType: "dateTime",
        yValueFormatString: "#0.0 h"
    }],        
}


function loadDailyGraph(steamid){
    if (steamid) {       
        $.getJSON("data.php", {
                type: "daily",
                steamid: steamid
            },
            function(data) {
                if (data["sum"]) {
                    dailyGraph.options.data[0].visible = true;
                    dailyGraph.options.data[0].dataPoints = data["sum"].map(function(data){
                        return {
                            x: new Date(data["date"]),
                            y: data["playtime"]/60.0
                        };
                    });
                } else {
                    dailyGraph.options.data[0].visible = false;
                }
                dailyGraph.render();
            }
        )
    }
}
