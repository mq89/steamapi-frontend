yearlyGraphOptions = {
    title: {
        text: "Jährliche Spielzeit",
        fontSize: 14
    },
    axisX: {
        title: "Jahr",
        valueFormatString: "YYYY",
        interval: 1,
        intervalType: "year",
        labelFontSize: 10,
        titleFontSize: 12
    },
    axisY: {
        title: "Spielzeit",
        valueFormatString: "0 h",
        minimum: 0,
        labelFontSize: 10,
        titleFontSize: 12
    },
    data: [
    {
        type: "column",
        color: "#123456",
        yValueFormatString: "#0.0 h"
    }]
}


function loadYearlyGraph(steamid) {
    if (steamid) {
        $.getJSON("data.php", {
                type: "yearly",
                steamid: steamid
            },
            function(data) {
                yearlyGraph.options.data[0].dataPoints = data["yearly"].map(function(data){
                    return {
                        x: new Date(data["year"]),
                        y: data["playtime"]/60.0
                    };
                });
                yearlyGraph.render();

            }
        )
    }
}
