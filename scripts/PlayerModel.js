function PlayerModel() {
    var self = this;
    self.players = ko.observableArray();
    self.selectedSteamid = ko.observable("");

    self.load = function() {
        $.getJSON("data.php",
        {
            type: "players"
        },
        function(data) {
            self.players(data.players);
        })
    }
}